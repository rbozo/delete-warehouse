# 仓库介绍

- 批量删除gitee仓库
- 两种规则删除

# 使用方法

```git
git clone https://gitee.com/rbozo/delete-warehouse.git
```
- 拉取代码到本地
- 当前目录下执行

```git
npm i
```
# 申请access_token 

- 填入app.js文件中
- 执行以下操作

```git
npm run serve
```
