/*
 * @Author: Harry
 * @Date: 2022-07-24 09:19:37
 * @LastEditors: harry
 * @Github: https://github.com/rr210
 * @LastEditTime: 2022-07-24 10:33:39
 * @FilePath: \gittee\app.js
 */
import fetch from "node-fetch";
const token = ''  // access_token
const username = ''  // 你的用户名称 必填
const rules = [''].map(v => username + '/' + v) // 填写排除删除仓库的名称，数组形式' 不使用不要填写
const rules2 = ''  // 填写需要删除的仓库，可定义为包含某个字符串即可
// https://gitee.com/api/v5/users/{username}/repos  获取某个用户的公开仓库
// https://gitee.com/api/v5/user/repos 可以获取所有的仓库列表
async function nameDe() {
    let resp = await fetch(`https://gitee.com/api/v5/user/repos?access_token=${token}&type=all&sort=full_name&direction=asc&page=1&per_page=100`, {
        "headers": {
            "accept": "application/json, text/plain, */*",
            "content-type": "application/json;charset=utf-8"
        },
        "referrer": "https://gitee.com/api/v5/swagger",
        "referrerPolicy": "strict-origin-when-cross-origin",
        "body": null,
        "method": "GET",
        "mode": "cors",
        "credentials": "include"
    });

    let list = await resp.json();
    console.log('仓库查询到', list.length);
    let beforeCheck = []
    if (rules.length > 0) {
        // 排除哪些不需要删除，其余的全部删除
        beforeCheck = list.filter((v, i) => !rules.includes(v.full_name));
    } else if(rules2) {
        // 根据某种规则删除
        beforeCheck = list.filter((v, i) => v.full_name.indexOf(rules2) > 0);
    }
    let fetchurls = beforeCheck.map((v, i) => 'https://gitee.com/api/v5/repos/' + v.full_name + `?access_token=${token}`)
    console.log(fetchurls)
    console.log(`根据规则${rules},一共查询到${fetchurls.length}条`);
    for (var i = 0, c = fetchurls.length; i < c; i++) {
        let url = fetchurls[i];
        await fetch(url, {
            method: 'DELETE',
            headers: {
                "Content-Type": "application/json;charset=UTF-8"
            }
        })
    }
    console.log('已删除成功')
}

nameDe()